using System.Collections;
using System.Collections.Generic;
// using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour
{
    protected Collision coll;
    protected Rigidbody2D rb;

    [SerializeField]
    protected float _speed = 10f;
    [SerializeField]
    protected float _jumpForce = 50f;
    [SerializeField]
    protected float _dashSpeed = 20f;
    [SerializeField]
    protected float _dashTime;
    [SerializeField]
    protected float _StartdashTime;
    [SerializeField]
    protected float _dashDirection;

    protected Vector2 dir;

    [SerializeField]
    protected bool isGrounded;
    [SerializeField]
    protected bool isDashing;

    [SerializeField]
    protected LayerMask groundLayer;

    //groundCheck
    [SerializeField]
    protected float collisionRadius = 0.25f;
    [SerializeField]
    protected Vector2 bottomOffset, rightOffset, leftOffset;

    protected void Walk(Vector2 dir)
    {
        rb.velocity = new Vector2(dir.x * _speed, rb.velocity.y);
    }

    protected void Jump(Vector2 dir)
    {
        rb.velocity = new Vector3(rb.velocity.x, 0);
        rb.velocity += dir * _jumpForce;
    }

    protected void Dash()
    {
        //Dashing
        if (isDashing)
        {
            rb.velocity = transform.right * _dashDirection * _dashSpeed;
            _dashTime -= Time.deltaTime;
            if (_dashTime <= 0)
            {
                isDashing = false;
            }
        }
    }
}