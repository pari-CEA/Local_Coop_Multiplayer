using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Camera _cam1;
    [SerializeField]
    private Camera _cam2;
    [SerializeField]
    private Player_1 _p_1;
    [SerializeField]
    private Player_2 _p_2;
    [SerializeField]
    private Vector3 _p1_Offset;
    [SerializeField]
    private Vector3 _p2_Offset;
    // Update is called once per frame
    void Update()
    {
        _cam1.transform.position = _p_1.transform.position + _p1_Offset;
        _cam2.transform.position = _p_2.transform.position + _p2_Offset;
    }
}
