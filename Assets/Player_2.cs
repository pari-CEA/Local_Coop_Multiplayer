using System.Collections;
using System.Collections.Generic;
// using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Player_2 : PlayerController
{
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        // walk
        float x = Input.GetAxis("p2horizontal");
        float y = Input.GetAxis("p2Vertical");
        Vector2 dir = new Vector2(x, y);
        Walk(dir);

        isGrounded = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);

        // Jump
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (isGrounded)
                Jump(Vector2.up);
        }

        //Dash state
        if (Input.GetKeyDown(KeyCode.RightShift) && !isGrounded && x != 0)
        {
            isDashing = true;
            _dashTime = _StartdashTime;
            rb.velocity = Vector2.zero;
            _dashDirection = x;
        }
        Dash();
    }
}
