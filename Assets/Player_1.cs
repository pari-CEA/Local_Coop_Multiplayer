using System.Collections;
using System.Collections.Generic;
// using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class Player_1 : PlayerController
{
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        // walk
        float x = Input.GetAxis("p1horizontal");
        float y = Input.GetAxis("p1Vertical");
        dir = new Vector2(x, y);
        Walk(dir);

        isGrounded = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);

        // Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
                Jump(Vector2.up);
        }

        //Dash state
        if (Input.GetKeyDown(KeyCode.LeftShift) && !isGrounded && x != 0)
        {
            isDashing = true;
            _dashTime = _StartdashTime;
            rb.velocity = Vector2.zero;
            _dashDirection = x;
        }

        Dash();
    }
}